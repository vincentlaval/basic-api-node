const mongoose = require('mongoose');

module.exports = async URI => {
  const conn = await mongoose.connect(URI, {
    useCreateIndex: true,
    useFindAndModify: false,
    useNewUrlParser: true,
    useUnifiedTopology: true
  });
  console.log(`MongoDB was started: ${conn.connection.host}`);
};