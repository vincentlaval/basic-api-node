const Data = require('../model/dataModel');
require('dotenv').config({ path: '../.env' });

exports.getData = async (req, res, next) => {
  const data = await Data.findById(req.params.id)
  res.status(200).json(data);
};

exports.getAllData = async (req, res, next) => {
  const data = await Data.find({});
  res.status(200).json(data);
};

exports.createData = async (req, res, next) => {
  const data = await Data.create(req.body);
  res.status(201).json({ data: data });
};

exports.deleteData = async (req, res, next) => {
  await Data.findByIdAndDelete(req.params.id);
  res.status(200).json({ status: 'delete success' });
};

exports.updateData = async (req, res, next) => {
  await Data.findByIdAndUpdate(req.params.id, req.body)
  res.status(200).json({ status: 'update success' })
}