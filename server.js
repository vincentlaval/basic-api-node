require('dotenv').config();
require('./database')(process.env.MONGO_URI)
const server = require('./app');

const PORT = process.env.PORT

server.listen(PORT, () => {
    console.log(`Server was connected on port ${PORT}`)
});

process.on('unhandledRejection', (err) => {
    console.log(err)
})