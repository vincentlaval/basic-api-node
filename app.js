const express = require('express')
const app = express()
const helmet = require('helmet')
const morgan = require('morgan')
const dataRoutes = require('./routes/dataRoutes')

app.use(express.json())
app.use(morgan('dev'))
app.use(express.urlencoded({ extended: true }))
app.use(helmet())

app.use('/api/data', dataRoutes)

module.exports = app