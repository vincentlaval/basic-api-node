const mongoose = require('mongoose');

const dataSchema = new mongoose.Schema(
  {
    fieldDate: {
      type: Date,
      default: Date.now
    },
    fieldString: {
      type: String,
      required: true,
      trim: true,
      unique: true
    },
    fieldBool: {
      type: Boolean,
      default: true
    },
    fieldEnum: {
      type: String,
      enum: ["enum1", "enum2"]
    },
    fieldRef: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'data',
    }
  }
)

module.exports = mongoose.model('data', dataSchema);
