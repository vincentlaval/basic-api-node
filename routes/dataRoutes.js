const router = require('express').Router();

const {
  getData,
  getAllData,
  createData,
  deleteData,
  updateData
} = require('../controllers/dataControllers');

router
  .route('/')
  .get(getAllData)
  .post(createData);

router
  .route('/:id')
  .get(getData)
  .put(updateData)
  .delete(deleteData);

module.exports = router;